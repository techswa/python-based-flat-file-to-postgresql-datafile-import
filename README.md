# README #

## Python based database file conversion and import into PostgresSQL ##

* Convert & Import State of Texas Medical Board Database to a PostgreSQL server for web access.

* The source file is fixed field length based

* Certain fields are removed instream before importing into the PostgreSQL table.

* Abridged input & output files included