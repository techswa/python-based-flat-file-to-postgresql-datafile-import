#!/usr/bin/python

# 03/02/2017 BB
# Version 1.0

# Converts full fixed length file of doctor information and imports to postgres

import psycopg2
from config import config


def main():
    print('called: main')

    # Test or Production mode
    testMode = False
    # Text file settings
    inputFileName = "/Users/swa/documents/dev/data-conversion/txmd/201702PHYALLF.txt"
    outputFileName = "/Users/swa/documents/dev/data-conversion/txmd/output.txt"
    textFileEncoding = 'utf-8'
    # SUe section just make reading long layouts a little easier
    fieldLayouts = []  # Holds the starting position of the fields - no offset
    fieldLayouts.extend([[1, 7], [8, 16], [17, 21], [22, 46], [47, 68], [
                        69, 71], [72, 101], [102, 131], [132, 151], [152, 153]])
    fieldLayouts.extend([[154, 163], [164, 193], [194,223], [224, 243], [244, 245], [
                        246, 255], [256, 263], [264, 293], [294, 323], [324, 353]])
    fieldLayouts.extend([[354, 420], [421, 424], [425, 426], [427, 434], [435, 435], [
                        436, 465], [466, 473], [474, 475], [476, 477]])
    fieldLayouts.extend([[478, 479], [480, 482], [483, 490], [491, 503],
                         [504, 504], [505, 507], [508, 508]])
    fieldLayouts = tuple(fieldLayouts)  # convert to tuple to lock
    print(fieldLayouts)
    # fieldStartingPos = fieldStartingPos1 + fieldStartingPos2 + fieldStartingPos3

    # PostgreSQL settings
    databaseName = "TXMB"
    tableName = "test"

    process_source_file(testMode, inputFileName, outputFileName,
                        textFileEncoding, databaseName, tableName, fieldLayouts)
    print('Main routine is ending ...')

# Reads text file and processes sinlge line
# closes at the end of process


def process_source_file(testMode, inputFileName, outputFileName, textFileEncoding, databaseName, tableName, fieldLayouts):
    print('called: process_source_file')
    try:
        with open(inputFileName, encoding=textFileEncoding) as fileobject:
            firstLine = True  # Create new file if this is the first line - otherwise append text
            # If ! testmode then connect to PostgreSQL
            if testMode is False:
                x = 1
                print('testMode is False')
                print('Creating dbConnection')
                dbConnection = pgsql_connect(databaseName, tableName)


            for line in fileobject:
                display_single_line(line)  # Display what we have just read

                # Setup line & field parsing
                fieldCount = len(fieldLayouts) # Get total number of fields to process
                field = 0 #start with first field

                recordTuple = parse_single_line(
                    line, fieldLayouts)  # Parse line into an array

                # Print the returned field count
                print(str(len(recordTuple)))

                # Check if last field to process otherwise increment to next field


                if testMode:  # Write to external text file instead of database
                    if firstLine:
                        # Overwrite or start new file
                        write_single_line(outputFileName, 'w', line)
                        firstLine = False  # append any additional lines
                    else:
                        # Append text file
                        write_single_line(outputFileName, 'a', line)

                else:  # Write file to postgresql
                    # Drop temporay table
                    # Create new tableName
                    print('Running database routines')
                    # Turn array into tuple for insertion
                #    recordTuple = create_record_tuple(parsedLineArray)

                    #insert into postgres server
                    result = insert_record(dbConnection, recordTuple)

                    #Commit record to postgres server
                    dbConnection.commit()

        #Close server connection
        pgsql_close(dbConnection)
        return None

    except Exception as e:
        raise


def display_single_line(line):
    print('called: display_single_line')
    print(line)

def insert_record(dbConnection, recordTuple):
    print("called: insert_record")
    # try:
    # dbConnection = pgsql_connect('TXMD', 'test')
    dbCursor = dbConnection.cursor()
    dbCursor.execute("INSERT INTO doctors (tx_id, license, filler, name_last, name_first,\
    name_suffix, address_mailing_line_1, address_mailing_line_2, address_mailing_city, address_mailing_state,\
    address_mailing_zipcode, address_practice_line_1, address_practice_line_2, address_practice_city,\
    address_practice_state, address_practice_zipcode, year_of_birth, birth_place, specialty_primary,\
    specialty_secondary, medical_school, medical_school_grad_year, medical_degree, license_issue_date,\
    license_method_code, reciprocity_state, license_expiration_date, practice_type_code, practice_setting_code,\
    practice_time_code, registration_status_code, registration_status_date, county_name, gender_code, race_code, hispanic_origin)\
     VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s\
     , %s, %s, %s, %s, %s, %s)", recordTuple)

    # except Exception as e:
    #     print(e)
    # raise
    return 'dd'


def parse_single_line(line, fieldLayouts):

    # Remove LF from end of line
    line = line[:-1]

    print('called: parsed_single_line')
    fieldIndex = 0  # start with the first field number

    # Tuple locations for data
    fieldCount = len(fieldLayouts) #How many fields to process
    fieldStartPosition = 0 # Starting position of field in line
    fieldLength = 0 #Stores calculated postion of the field end
    fieldText = "" #Store current field in this
    fieldsTuple = () #This tuple holds the fields to be returned

    print("Field count: {}".format(fieldCount))

    for field in fieldLayouts:
        print('Processing field: {}'.format(fieldIndex))
        print('Field pair: {}'.format(fieldLayouts[fieldIndex]))

        # Retreive the field position to start extraction
        fieldStartPostition = fieldLayouts[fieldIndex][0] - 1
        print('fieldStartPostition: {}'.format(fieldStartPostition))

        # How many charactors will be extracted
        fieldLength = fieldLayouts[fieldIndex][1]
        print('fieldLength: {}'.format(fieldLength))

        # Extract the text from field location in the line
        fieldText = line[fieldStartPostition:fieldLength]
        #Strip excess white space
        fieldText = fieldText.strip()

        # Display the extracted field text
        print('Extracted: {}'.format(fieldText))

        #incase fieldText is null - add '' to it
        fieldText = fieldText + ''
        # Create tuple of fields to be returned
        fieldsTuple = fieldsTuple + tuple([fieldText])
        print(fieldsTuple)
    # Increment field counter
        fieldIndex += 1
        #Have we processed the complete line?
        #If so, then break out process
        if (fieldIndex == fieldCount):
            break
    return fieldsTuple


# Write a single line to the disk
# Takes:
# - fully qualified path & filename
# - operation: overwrite, append
# - line to write
def write_single_line(outputFileName, fileOperation, line):
    print('called: write_single_line')
    # fileName=open(OutputFileName, fileOperation)
    # print line, file=fileName
    file = open(outputFileName, fileOperation)
    file.write(line)

# Return the record count


def get_record_count(dbConnection, tableName):
    print('called: get_record_count')
    try:
        # dbConnection = pgsql_connect('TXMD', 'test')
        dbCursor = dbConnection.cursor()

        sql = """select * from {};""".format(tableName)
        print(sql)
        dbCursor.execute(sql)
        rows = dbCursor.fetchall()
        print(rows)
        for row in rows:
            print('Results are:', row[1])
    except Exception as e:
        print(e)
        # raise

#  Connect to postgresql server
# Open database connection


def pgsql_connect(databaseName, tableName):
    """ Connect to the PostgreSQL database server """
    dbConnection = None
    try:
        # read connection parameters
        params = config()
        print(params)
        # update database setting
        params['database'] = databaseName
        print(params)
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        dbConnection = psycopg2.connect(**params)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        return dbConnection

# Close database connection


def pgsql_close(dbConnection):
    if dbConnection is not None:
        dbConnection.close()
        print('Database connection closed.')


if __name__ == '__main__':
    main()
